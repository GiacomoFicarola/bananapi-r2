![BananaPi R2](./Images/BananaPiR2.jpg)

## These are some of the .img files I want to share.

They are raspbian images updated to the latest kernel-based linux kernel from the following , [git](https://github.com/frank-w/BPI-R2-4.14) thanks to frank-w.

The files have been cleaned up by me personally to be as light as possible in memory. (command line only, I personally consider it more useful for this type of machine)

I personally installed, later on my image:
 - [Format Hard Disk](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/5dce7bc17eed65b21c44598b779c27e8a0c178dc/Tips/Format_Hard_Disk.txt)
 - [Connected](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/5dce7bc17eed65b21c44598b779c27e8a0c178dc/Script%20bash/Mount_Script) a network hard disk 
 - Installed a DNS server, thanks to [AdGuard Home](https://github.com/AdguardTeam/AdGuardHome)
 - Changed the ssh keys in [ec25519](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/943fb0c6c1c09ca79edf4798551cb41e0dcd51aa/Tips/Key_Gen_ec25519.txt) or [RSA 4096 bits](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/943fb0c6c1c09ca79edf4798551cb41e0dcd51aa/Tips/Key_Gen_RSA4096.txt)
 - Installed a Torrent client, [Deluge](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/ea8dce4bd6ff1fb97885d20be0db2a6412217a5e/Tips/Deluge_Install_Command.txt)
 - [Disabled the ping](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/8ef3a33b22dda108e15ce7d14a82ab190f94f540/Tips/Disable_Ping.txt)
 - Created a [bash](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/8ef3a33b22dda108e15ce7d14a82ab190f94f540/Script%20bash/Backup_Script) script to make periodic backups of the whole micro sd hosting this system image
 - Created a [script](https://gitlab.com/GiacomoFicarola/bananapi-r2/-/blob/5dce7bc17eed65b21c44598b779c27e8a0c178dc/Python%20script/ip.py) to know what my IP is when I'm away from home

***Make good use of it!***

sources:
*  https://github.com/frank-w/BPI-Router-Linux
*  https://github.com/BPI-SINOVOIP/BPI-R2-bsp
*  https://github.com/BPI-SINOVOIP/BPI-R2-bsp-4.14
*  https://github.com/AdguardTeam/AdGuardHome

File:
   * [.deb packages for update kernel](https://github.com/frank-w/BPI-Router-Linux/releases), you can update the kernel with the command "sudo dpkg -i package.deb".

Git is constantly undergoing changes, look at commits to see previous projects or programs taken from the main git page.

![AutoMountUSB](./Images/AutoMountUSB.png)