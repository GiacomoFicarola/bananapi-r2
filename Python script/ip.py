import botogram
import requests

bot = botogram.create("API_TOKEN")

@bot.command("IP")
def IP_message(chat, message, args):
    if message.sender.id == YOUR_ID_TELEGRAM:
        url = "https://api.myip.com"
        try:
            response = requests.get(url)
            data = response.json()
            ip_address = data["ip"]
            country = data["country"]
            cc = data["cc"]

            reply_text = f"IP: {ip_address}\nCountry: {country}\nCountry Code: {cc}"
            chat.send(reply_text)

        except Exception as e:
            chat.send(f"Error: {str(e)}")

bot.run()